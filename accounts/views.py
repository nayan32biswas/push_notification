from django.shortcuts import render, get_object_or_404
from rest_framework import status
from rest_framework_jwt.views import ObtainJSONWebToken
from django.db.models import Q
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import (AllowAny, IsAuthenticated)
from rest_framework.generics import (
    CreateAPIView,
    RetrieveAPIView,
    ListAPIView,

)
from django.contrib.auth import get_user_model

# Create your views here.
from .serializers import (
    UserDetailSerializer,
    UserLoginSerializer,
    UserRegistrationSerializer,
    UserListSerializer,
    MessageSerializer
)
from .models import Message

User = get_user_model()


class UserRegistrationAPIView(CreateAPIView):
    serializer_class = UserRegistrationSerializer
    queryset = User.objects.all()
    permission_classes = [AllowAny]


class UserLoginAPIView(ObtainJSONWebToken):
    permission_classes = [AllowAny]

    def post(self, request, *args, **kwargs):
        data = request.data
        if(data.get("username") is None or data.get("password") is None):
            return Response({"message": "Requred field is empty"}, status=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION)
        response = super().post(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            user = User.objects.get(username=request.data[User.USERNAME_FIELD])
            response.data.update({"email": user.email})
            return response
        response.data.update(dict({"message": "Username or Password doesnot match"}))
        return response


class UserProfileDetailAPIView(RetrieveAPIView):
    queryset = User.objects.all()
    permission_classes = [AllowAny]
    lookup_field = 'username'
    serializer_class = UserDetailSerializer


class UserListAPIView(ListAPIView):
    queryset = User.objects.all()
    permission_classes = [AllowAny, ]
    serializer_class = UserListSerializer


class UserMessageListAPIView(ListAPIView):
    lookup_fields = "username"
    permission_classes = [IsAuthenticated]
    serializer_class = MessageSerializer

    def get_queryset(self, *args, **kwargs):
        user = self.request.user
        username = self.kwargs[self.lookup_fields]
        receiver = get_object_or_404(User, username=username)
        queryset_list = Message.objects.filter(
            (Q(receiver=receiver) & Q(sender=user) |
             Q(receiver=user) & Q(sender=receiver))
        )
        return queryset_list


class UserMessageCreateAPIView(APIView):
    lookup_fields = "username"
    permission_classes = [IsAuthenticated]

    def post(self, *args, **kwargs):
        data = self.request.data
        user = self.request.user
        username = self.kwargs[self.lookup_fields]
        receiver = get_object_or_404(User, username=username)
        message = data.get("message")
        if len(message) and user != receiver:
            Message.objects.create(
                receiver=receiver,
                sender=user,
                message=message
            )
            return Response({"message": "created"})
        return Response({"message": "Something wrong"})
