import jwt
import json
import datetime
from django.conf import settings
from django.contrib.auth import get_user_model
from channels.consumer import AsyncConsumer
from channels.db import database_sync_to_async

from accounts.models import Message
from accounts.serializers import MessageSerializer

User = get_user_model()


"""
to use socket IO use <<AsyncConsumer>> to work asynchronous.
if we use <<await>> then parrent function should be <<async>>
Must use database_sync_to_async and use as decorator to read something from database. 
And use async and await when we called database_sync_to_async decorator function.
"""


class ChatConsumer(AsyncConsumer):
    async def send_websocket_data(self,  data):
        await self.send({"type": "websocket.send", "text": json.dumps(data)})

    # build in function
    async def websocket_connect(self, event):
        await self.send({"type": "websocket.accept", "token": "token"})
        self.user = None
        self.chat_room = self.scope["url_route"]["kwargs"]["chat_room"]
        self.sender, self.receiver = self.chat_room.split("__")

        await self.channel_layer.group_add(self.chat_room, self.channel_name)
        send_data = {"message": "ChatRoom created"}
        await self.send_websocket_data(send_data)

    def extract_user(self, token):
        if token:
            try:
                payload = jwt.decode(
                    token,
                    settings.SECRET_KEY,
                    algorithms=[settings.JWT_ALGORITHM]
                )
            except (jwt.DecodeError, jwt.ExpiredSignatureError):
                return ValueError({'message': 'Token is invalid'}, status=400)
            username = payload['username']
            return username
        return None

    async def check_auth(self, headers):
        if headers:
            token = headers.get("token")
        self.username = self.extract_user(token)
        if (self.username == self.receiver):
            self.sender, self.receiver = self.receiver, self.sender
        self.user = await self.get_user(self.username)
        self.receiver_user = await self.get_user(self.receiver)

    # build in function
    # this function called one time when some message onthe room
    async def websocket_receive(self, event):
        received_data = event.get("text")
        if received_data is not None:
            received_dict = json.loads(received_data)
            await self.check_auth(received_dict.get("headers"))
            if self.user is not None and received_dict.get("data"):
                message = received_dict.get("data").get("message")

                message_data = await self.create_message(message)
                message_data["__type__"] = "MESSAGE"

                await self.channel_layer.group_send(
                    self.chat_room,
                    {
                        "type": "chat_message",
                        "send_data": message_data
                    }
                )

    # this is user define function. called based on type when type==chat_message
    # this function call multiple time based on number of user on the room
    # and assigned data change based on user data
    async def chat_message(self, event):
        send_data = event.get("send_data")
        await self.send_websocket_data(send_data)

    # build in function
    async def websocket_disconnect(self, event):
        print("disconnect", event)

    # this remove too many request or memory loss or something bad
    @database_sync_to_async
    def get_message(self, other_user):
        return other_user

    @database_sync_to_async
    def get_user(self, username):
        user = User.objects.filter(username=username).first()
        return user

    @database_sync_to_async
    def create_message(self, message):
        if len(message) and self.user != self.receiver_user:
            message_instance = Message.objects.create(
                receiver=self.receiver_user,
                sender=self.user,
                message=message
            )
            data = MessageSerializer(message_instance).data
            return data
