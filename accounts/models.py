from django.db import models
from django.core.exceptions import ValidationError
# Create your models here.
from django.contrib.auth import get_user_model

User = get_user_model()


class Message(models.Model):
    sender = models.ForeignKey(User, related_name="message_senders", on_delete=models.CASCADE)
    receiver = models.ForeignKey(User, related_name="message_receivers", on_delete=models.CASCADE)
    message = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

    def clean(self):
        if self.sender == self.receiver:
            raise ValidationError({'message': "User can't be same"}, code='invalid')
