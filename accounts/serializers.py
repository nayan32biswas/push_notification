from rest_framework import serializers
from django.core.exceptions import ValidationError
from django.conf import settings
from django.contrib.auth import get_user_model
from .models import Message

User = get_user_model()


class MessageSerializer(serializers.ModelSerializer):
    sender = serializers.SerializerMethodField()
    receiver = serializers.SerializerMethodField()

    class Meta:
        model = Message
        fields = "__all__"
    extra_kwargs = {
        "created_at": {"read_only": True}
    }

    def get_sender(self, message):
        return message.sender.username

    def get_receiver(self, message):
        return message.receiver.username


class MessageCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ("message")


class UserListSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            "username",
            "first_name",
        )


class UserDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            "email",
            "username",
            "first_name",
            "last_name",
        )


class UserRegistrationSerializer(serializers.ModelSerializer):
    password = serializers.CharField(label='Password')
    confirm_password = serializers.CharField(label='Confirm Password')

    class Meta:
        model = User
        fields = (
            "first_name",
            "last_name",
            "email",
            "username",
            'password',
            'confirm_password',
        )
        extra_kwargs = {
            "password": {"write_only": True},
            "confirm_password": {"write_only": True}
        }

    def validate_username(self, value):
        data = self.get_initial()
        print("in username")
        username = data.get("username")
        user_qs = User.objects.filter(username=username)
        if user_qs.exists():
            raise ValidationError({"message": "Username already used."})
        return value

    def validate_email(self, value):
        data = self.get_initial()
        print("in email")
        email = data.get("email")
        user_qs = User.objects.filter(email=email)
        if user_qs.exists():
            raise ValidationError({"message": "This email already used."})
        return value

    def validate_confirm_password(self, value):
        data = self.get_initial()
        print("in password")
        password = data.get("confirm_password")
        confirm_password = value
        if password != confirm_password:
            raise ValidationError({"message": "Passwords must match."})
        return value

    def create(self, validated_data):
        username = validated_data['username']
        first_name = validated_data['first_name']
        last_name = validated_data['last_name']
        email = validated_data['email']
        password = validated_data['password']

        user_obj = User(username=username, first_name=first_name, last_name=last_name, email=email)
        user_obj.set_password(password)
        user_obj.save()
        self.user = user_obj
        return validated_data


class UserLoginSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            "username",
            "password",
        ]

    def validate(self, data):
        return data
