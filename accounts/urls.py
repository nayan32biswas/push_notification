from django.urls import path
from .views import (
    UserRegistrationAPIView,
    UserLoginAPIView,
    UserProfileDetailAPIView,
    UserListAPIView,
    UserMessageListAPIView,
    UserMessageCreateAPIView
)
urlpatterns = [
    path('registration/', UserRegistrationAPIView.as_view(), name="signup"),
    path('login/', UserLoginAPIView.as_view(), name="login"),
    path('list/', UserListAPIView.as_view(), name="list"),
    path('message/<username>/', UserMessageListAPIView.as_view(), name="message_list"),
    path('create/message/<username>/', UserMessageCreateAPIView.as_view(), name="message_create"),
    path('<username>/', UserProfileDetailAPIView.as_view(), name="detail")
]
