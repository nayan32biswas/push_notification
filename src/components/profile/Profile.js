import React, { Component } from 'react'
import { Redirect } from "react-router-dom"
import { connect } from 'react-redux'

import Message from "../message/Message"
export class Profile extends Component {
    render() {
        if (!this.props.isAuthenticated) {
            return (
                <Redirect to="/" />
            )
        }
        return (
            <div>
                <h1>Profile view</h1>
                <Message
                    username={this.props.username}
                />
            </div>
        )
    }
}

const mapStateToProps = (state, props) => {
    const username = props.match.params.username
    return {
        username,
        isAuthenticated: state.auth.isAuthenticated,
        user: state.auth.user,
    }
}
const mapDispatchToProps = {
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile)
