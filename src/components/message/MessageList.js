import React from 'react'

export default function MessageList(props) {
    return (
        <div >
            {
                props.messageList ?
                    props.messageList.map((message, key) => {
                        return (
                            <div className="row" key={key}>
                                <div>
                                    <p>{message.message} by {message.sender}</p>
                                    <hr />
                                </div>
                            </div>
                        )
                    })
                    : null
            }
        </div>
    )
}
