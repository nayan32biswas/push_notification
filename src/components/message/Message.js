import React, { Component } from 'react'
import { connect } from 'react-redux'

import store from "../../store"
import { FETCH_NEW_MESSAGE } from "../../store/types"
import { wsSocket, wsSendMessage } from "../../store/actions/webSocketActions"
import { fetchMessageList, sendMessage } from "../../store/actions/messageActions"

import MessageList from "./MessageList"



export class Message extends Component {
    state = {
        data: {
            message: ""
        },
        URL: `ws://127.0.0.1:8000/ws/chat/nayan/`
    }
    getAuthToken = () => {
        const token = localStorage.getItem('auth_token');
        return token ? token : "";
    }
    componentDidMount() {
        const users = [this.props.auth_username, this.props.username]
        users.sort()
        const chatRoom = `${users[0]}__${users[1]}`
        const URL = `ws://127.0.0.1:8000/ws/chat/${chatRoom}/`

        // console.log(URL)

        this.setState({ URL })
        // const token = this.getAuthToken()
        this.ws = new WebSocket(URL)

        this.props.fetchMessageList(this.props.username)
        // ------------------------------------------------
        this.ws.onopen = () => {
            console.log('connected')
        }
        this.ws.onmessage = event => {
            let data = event.data
            data = JSON.parse(data)
            // console.log(data)
            if (data.__type__ && data.__type__ === "MESSAGE") {
                delete data["__type__"]
                store.dispatch({ type: FETCH_NEW_MESSAGE, payload: data })
            }
        }
        this.ws.onclose = () => {
            console.log('disconnected')
            this.setState({ ws: new WebSocket(URL) })
        }
    }
    handelMessageChange = (event) => {
        this.setState({
            data: {
                ...this.state.data,
                message: event.target.value
            }
        })
    }
    onSubmit = (event) => {
        event.preventDefault()
        const sendData = {
            data: { ...this.state.data },
            headers: { "token": this.getAuthToken() }
        }
        // this.props.sendMessage(this.props.username, sendData)
        this.ws.send(JSON.stringify(sendData))
        this.setState({
            data: {
                ...this.state.data,
                message: ""
            }
        })
    }
    render() {
        return (
            <div className="container">
                <div>
                    <MessageList
                        messageList={this.props.messageList}
                    />
                </div>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <textarea
                            onChange={this.handelMessageChange}
                            value={this.state.data.message}
                            type="text" className="form-control" id="message" placeholder="message"
                        />
                    </div>
                    <button onClick={this.onSubmit}>Send</button>
                </form>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        auth_username: state.auth.user.username,
        messageList: state.message.messageList
    }
}

const mapDispatchToProps = {
    wsSocket,
    wsSendMessage,
    sendMessage,
    fetchMessageList,
}

export default connect(mapStateToProps, mapDispatchToProps)(Message)
