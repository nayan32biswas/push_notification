import React, { Component } from 'react'
import { Router, Route, Switch } from 'react-router-dom'
import history from "../history"
import Navbar from "./navbar/Navbar"
import Home from "./home/Home"

import Logout from "./auth/Logout"
import Registration from "./auth/Registration"
import Login from "./auth/Login"

import Profile from "./profile/Profile"

export default class App extends Component {
  render() {
    return (
      <Router history={history}>
        <Navbar />
        <br />
        <div className="container">
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/logout/" exact component={Logout} />
            <Route path="/login/" exact component={Login} />
            <Route path="/registration/" exact component={Registration} />


            <Route path="/user/:username/" exact component={Profile} />

          </Switch>
        </div>
      </Router>
    )
  }
}
