import React, { Component, Fragment } from 'react'
import { Link } from "react-router-dom"
import { connect } from 'react-redux'

import { fetchUsers } from "../../store/actions/authActions"

export class Home extends Component {
    componentDidMount() {
        this.props.fetchUsers()
    }
    render() {
        return (
            <Fragment>
                <div>
                    {
                        this.props.userList.map((user, key) => {
                            return (
                                <div key={key}>
                                    <Link to={`/user/${user.username}`}>
                                        {user.username}
                                    </Link>
                                </div>
                            )
                        })
                    }
                </div>
            </Fragment>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        userList: state.auth.userList
    }
}

const mapDispatchToProps = {
    fetchUsers
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)
