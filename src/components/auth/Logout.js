import React, { Component } from 'react'
import { connect } from 'react-redux'
import { logout } from "../../store/actions/authActions"
// import { removeToken } from "../../store/actions/tokenActions"
// import history from "../../history"

export class Logout extends Component {
    componentDidMount() {
        this.props.logout()
    }
    render() {
        this.props.logout()
        return (
            <div>
                Logout
            </div>
        )
    }
}
const mapDispatchToProps = {
    logout: logout
}

export default connect(null, mapDispatchToProps)(Logout)
