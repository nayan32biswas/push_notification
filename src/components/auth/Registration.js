import React, { Component } from 'react'
import { connect } from 'react-redux'
import { registration } from "../../store/actions/authActions"
import { emailIsValid } from "../../utils"

export class Registration extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user: {
                first_name: "",
                last_name: "",
                email: "",
                username: "",
                password: "",
                confirm_password: ""
            },
            is_valid_email: null,
            password_match: null
        }
    }
    handleFirstNameChange = (event) => {
        var first_name = event.target.value
        this.setState({ user: { ...this.state.user, first_name } })
    }
    handleLastNameChange = (event) => {
        var last_name = event.target.value
        this.setState({ user: { ...this.state.user, last_name } })
    }
    handleEmailChange = (event) => {
        var email = event.target.value
        this.setState({ user: { ...this.state.user, email } })
        if (emailIsValid(email)) {
            this.setState({ is_valid_email: true })
        }
        else {
            this.setState({ is_valid_email: false })
        }
    }
    handleUsernameChange = (event) => {
        var username = event.target.value
        this.setState({ user: { ...this.state.user, username } })
    }
    handlePasswordChange = (event) => {
        var password = event.target.value
        if (this.state.password_match !== null) {
            if (this.state.user.confirm_password !== password) {
                this.setState({ password_match: false })
            } else {
                this.setState({ password_match: true })
            }
        }
        this.setState({ user: { ...this.state.user, password } })
    }
    handleConfirmPasswordChange = (event) => {
        var confirm_password = event.target.value
        this.setState({ user: { ...this.state.user, confirm_password } })
        if (this.state.user.password !== confirm_password) {
            this.setState({ password_match: false })
        } else {
            this.setState({ password_match: true })
        }
    }
    onSubmitHandler = (event) => {
        event.preventDefault()
        this.setState({ is_valid_email: false })
        const user = this.state.user
        if (emailIsValid(user.email)) {
            this.setState({ is_valid_email: true })
            if (user.first_name && user.last_name) {
                if (user.password === user.confirm_password) {
                    // put your code here.
                    this.props.registration(user)
                }
            }
        }

    }
    render() {
        return (
            <div>
                {this.renderForm()}
            </div>
        )
    }
    renderForm() {
        return (
            <div className="container">
                <form onSubmit={this.onSubmitHandler}>
                    <div className="form-row">
                        <div className="col">
                            <label >First Name</label>
                            <input
                                onChange={this.handleFirstNameChange}
                                value={this.state.first_name}
                                type="email" className="form-control" placeholder="First Name"
                            />
                        </div>
                        <div className="col">
                            <label >Last Name</label>
                            <input
                                onChange={this.handleLastNameChange}
                                value={this.state.last_name}
                                type="email" className="form-control" placeholder="Last Name"
                            />
                        </div>
                    </div>
                    <div className="form-group">
                        <label >Email address</label>
                        <input
                            onChange={this.handleEmailChange}
                            value={this.state.email}
                            type="email" className="form-control" placeholder="Enter email"
                        />
                        <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                        {this.email_validation_message()}
                    </div>
                    <div className="form-group">
                        <label >Username</label>
                        <input
                            onChange={this.handleUsernameChange}
                            value={this.state.username}
                            type="email" className="form-control" placeholder="Username"
                        />
                    </div>
                    <div className="form-group">
                        <label >Password</label>
                        <input
                            onChange={this.handlePasswordChange}
                            value={this.state.password}
                            type="password" className="form-control" placeholder="Password"
                        />
                        <small id="emailHelp" className="form-text text-muted">Do not use common password</small>
                    </div>
                    <div className="form-group">
                        <label >Confirm Password</label>
                        <input
                            onChange={this.handleConfirmPasswordChange}
                            value={this.state.confirm_password}
                            type="password" className="form-control" placeholder="Confirm Password"
                        />
                        {this.password_match_message()}
                    </div>
                    <button onClick={this.onSubmitHandler} type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>
        )
    }
    email_validation_message() {
        if (this.state.is_valid_email === false)
            return (<small className="text-danger">You have entered an invalid email address!</small>)
        return null
    }
    password_match_message() {
        if (this.state.password_match === false)
            return (<small className="text-danger">Password does not match!</small>)
        return null
    }
}
const mapStateToProps = (state) => {
    return { state: state }
}
const mapDispatchToProps = {
    registration: registration
}
export default connect(mapStateToProps, mapDispatchToProps)(Registration)
