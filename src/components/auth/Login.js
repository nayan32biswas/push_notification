import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from "react-router-dom"
import { login } from "../../store/actions/authActions"
import history from "../../history"

export class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user: {
                username: "",
                password: ""
            },
            button_pressed: false
        }
    }
    handleUsernameChange = (event) => {
        var username = event.target.value
        this.setState({ user: { ...this.state.user, username } })
    }
    handlePasswordChange = (event) => {
        var password = event.target.value
        this.setState({ user: { ...this.state.user, password: password } })
    }
    onSubmitHandler = (event) => {
        event.preventDefault()
        if (this.props.isAuthenticated) {
            history.goBack()
        }

        if (this.state.user.username && this.state.user.password) {
            this.props.login(this.state.user, this.props.nextUrl)
        }
    }
    render() {
        return (
            <div className="container">
                <form onSubmit={this.onSubmitHandler}>
                    <div className="form-group">
                        <label>Username</label>
                        <input
                            onChange={this.handleUsernameChange}
                            value={this.state.username}
                            type="text" className="form-control" placeholder="Username"
                        />
                    </div>
                    <div className="form-group">
                        <label>Password</label>
                        <input
                            onChange={this.handlePasswordChange}
                            value={this.state.password}
                            type="password" className="form-control" placeholder="Password"
                        />
                    </div>
                    <div>
                        <button onClick={this.onSubmitHandler} type="submit" className="btn btn-primary">Submit</button>
                        <button className="btn float-right">
                            <Link  to="/registration/">Registration</Link>
                        </button>
                    </div>
                </form>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        isAuthenticated: state.auth.isAuthenticated,
    }
}

const mapDispatchToProps = {
    login: login
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)
