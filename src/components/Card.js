import React from 'react'
import { Link } from "react-router-dom"

export default function Card(props) {
    return (
        <div className="card">
            
            {
                props.show_user ?
                    <div className="card-header">
                        <Link to={`/user/${props.repository.user.username}/`}>{props.repository.user.username}</Link>
                    </div>
                    : null
            }

            <div className="card-body">
                <h5 className="card-title">{props.repository.name}</h5>
                <p className="card-text">{props.repository.description}</p>
                <Link to={`/repository/details/${props.repository.id}/`} className="btn btn-primary">View Details</Link>
            </div>
        </div>
    )
}
