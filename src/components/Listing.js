import React from 'react'
import Card from "./Card"
export default function Listing(props) {
    return (
        <div >
            {
                props.repository_list.map((repository, key) => {
                    return (
                        <div key={key}>
                            <Card
                                show_user={props.show_user}
                                repository={repository}
                            />
                            <br/>
                        </div>
                    )
                })
            }
        </div>
    )
}
