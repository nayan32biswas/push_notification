import React, { Component } from 'react'
import { Link } from "react-router-dom"
import { connect } from "react-redux"
import { logout } from '../../store/actions/authActions'

export class Navbar extends Component {

    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light h-100">
                <Link to="/" className="navbar-brand" >Push Notification</Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav ml-auto ">

                        {this.renderAdmin()}
                    </ul>
                </div>
            </nav>
        )
    }
    renderAdmin() {
        if (this.props.user && this.props.isAuthenticated) {
            const { username } = this.props.user
            return (
                <>
                    <li className="nav-item active"><Link className="nav-link" to="/logout/">Logout</Link></li>
                    <li className="nav-item active"><Link className="nav-link" to={`/user/${username}/`}>{username}</Link></li>
                </>
            )
        }
        else {
            return (
                <>
                    <li className="nav-item active"><Link className="nav-link" to="/login">Login</Link></li>
                    <li className="nav-item active"><Link className="nav-link" to="/registration">Registration</Link></li>
                </>
            )
        }
    }
}

const mapStateToProps = (state) => {
    return {
        isAuthenticated: state.auth.isAuthenticated,
        user: state.auth.user
    }
}

const mapDispatchToProps = {
    logout: logout
}

export default connect(mapStateToProps, mapDispatchToProps)(Navbar)

