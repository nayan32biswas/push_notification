export const isEmptyObj = (obj) => {
    for (var key in obj)
        if (obj.hasOwnProperty(key))
            return false
    return true
}


export const incDate = (date, inc) => {
    date = new Date(date)
    date.setDate(date.getDate() + inc)
    return date
}

export const emailIsValid = (email) => {
    var re = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
    if (re.test(email)) {
        return true
    }
    return false
}