// import baseApi from "../../api/baseApi"

export const wsSocket = (username) => dispatch => {
    const endPoint = `ws://127.0.0.1:8000/ws/chat/${username}/`
    let socket = new WebSocket(endPoint)

    socket.onmessage = function (e) {
        console.log("message: ", e)
    }
    socket.onopen = function (e) {
        console.log("onopen: ", e)
    }
    socket.onerror = function (e) {
        console.log("onerror: ", e)
    }
    socket.onclose = function (e) {
        console.log("onclose: ", e)
    }
}


export const wsSendMessage = (username, data) => dispatch => {
    const endPoint = `ws://127.0.0.1:8000/ws/chat/${username}/`
    let socket = new WebSocket(endPoint)
    
    socket.onmessage = function (e) {
        console.log("message: ", e)
    }
    socket.onopen = function (e) {
        console.log("onopen: ", e)
    }
    socket.onerror = function (e) {
        console.log("onerror: ", e)
    }
    socket.onclose = function (e) {
        console.log("onclose: ", e)
    }
}