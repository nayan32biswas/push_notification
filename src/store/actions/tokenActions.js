import jwtDecode from 'jwt-decode'
import store from "../index"
import { LOGIN } from "../types"
import { incDate } from "../../utils"

export const saveToken = (token = "", username = "") => {
    localStorage.setItem('auth_token', token)
    localStorage.setItem('username', username)
}

export const removeToken = () => {
    localStorage.removeItem('auth_token')
    localStorage.removeItem('username')
}

export const setAuthToken = (token = "", username = "") => {

    // removeToken()

    if (!token || !username) {
        token = localStorage.getItem('auth_token')
        username = localStorage.getItem('username')
        if (token) {
            let decode = jwtDecode(token);
            let date = incDate(new Date(), 1)

            if (new Date(decode.exp * 1000) < date) {
                localStorage.removeItem("auth_token")
            }
        }
    }
    else {
        saveToken(token, username)
    }
    if (token && username) {
        let decode = jwtDecode(token)
        decode = { ...decode, username: username }
        store.dispatch({ type: LOGIN, flashMessage: null, payload: { user: decode } })
    }
}