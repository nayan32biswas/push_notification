import jwtDecode from 'jwt-decode'
import {
    LOGOUT,
    REGISTRATION,
    FETCH_USERS,
} from '../types'
import baseApi from "../../api/baseApi"
import history from "../../history"
import { removeToken, setAuthToken } from "./tokenActions"


function errorHandle(error) {
    if (error.response) {
        console.log(error.response)
    }
    else {
        console.log("connection error")
    }
}

export const registration = (user) => dispatch => {
    baseApi.post('/auth/registration/', user).then(response => {
        dispatch({ type: REGISTRATION, payload: { error: response } })
        history.push('/login/')
    }).catch(error => {
        if (error.response) {
            let data = error.response.data
            if (data.username) {
                alert(data.username[0])
            }
            else if (data.email && data.email.message) {
                alert(data.email.message[0])
            }

        }
        else {
            alert("Connection error")
        }
    })
}
export const login = (user) => dispatch => {
    console.log(user)
    baseApi.post("/auth/login/", user).then(response => {
        let token = response.data.token, username;
        let decode = jwtDecode(token)
        decode = { ...decode }
        username = decode.username
        setAuthToken(token, username)
        history.push("/home/")
    }).catch(error => {
        errorHandle(error)
    })
}

export const fetchUsers = () => dispatch => {
    baseApi.get("/auth/list/").then(response => {
        dispatch({ type: FETCH_USERS, payload: { data: response.data } })
    }).catch(error => {
        errorHandle(error)
    })
}

export const logout = () => dispatch => {
    console.log("in logout")
    dispatch({ type: LOGOUT, flashMessage: null, payload: { user: {} } })
    removeToken()
    history.push('/login/')
}