import {
    FETCH_MESSAGE_LIST,
} from "../types"
import baseApi from "../../api/baseApi"


function errorHandle(error) {
    if (error.response) {
        console.log(error.response)
    }
    else {
        console.log("connection error")
    }
}

export const sendMessage = (username, data) => dispatch => {
    baseApi.post(`/auth/create/message/${username}/`, data).then(response => {
        baseApi.get(`/auth/message/${username}/`).then(response => {
            dispatch({ type: FETCH_MESSAGE_LIST, payload: { data: response.data } })
        }).catch(error => {
            errorHandle(error)
        })
    }).catch(error => {
        errorHandle(error)
    })
}


export const fetchMessageList = (username) => dispatch => {
    baseApi.get(`/auth/message/${username}/`).then(response => {
        dispatch({ type: FETCH_MESSAGE_LIST, payload: { data: response.data } })
    }).catch(error => {
        errorHandle(error)
    })
}

