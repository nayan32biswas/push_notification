import {
    LOGIN,
    LOGOUT,
    REGISTRATION,
    FETCH_USERS,

} from "../types"


const init = {
    isAuthenticated: false,
    user: {},
    userList: []
}
export default (state = init, action) => {
    switch (action.type) {
        case LOGIN: {
            return {
                ...state,
                user: action.payload.user,
                isAuthenticated: Object.keys(action.payload.user).lenth !== 0,
                error: {},
                flashMessage: action.flashMessage
            }
        }
        case LOGOUT: {
            console.log("in logout reducer")
            return { ...state, user: action.payload.user, isAuthenticated: false, error: {}, flashMessage: action.flashMessage }
        }
        case REGISTRATION: {
            return { ...state, user: {} }
        }
        case FETCH_USERS: {
            return {
                ...state,
                userList: action.payload.data
            }
        }

        default: {
            return state;
        }
    }
}