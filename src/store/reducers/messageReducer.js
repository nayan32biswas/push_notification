import {
    SEND_MESSAGE,
    FETCH_MESSAGE,
    FETCH_NEW_MESSAGE,
    FETCH_MESSAGE_LIST,
} from "../types"


const init = {
    sendMessage: null,
    message: null,
    messageList: null,
}

export default (state = init, action) => {
    switch (action.type) {
        case SEND_MESSAGE: {
            return {
                ...state,
            }
        }
        case FETCH_MESSAGE: {
            return {
                ...state,
            }
        }
        case FETCH_NEW_MESSAGE: {
            const new_message = action.payload
            
            return {
                ...state,
                messageList: [...state.messageList, new_message]
            }
        }
        case FETCH_MESSAGE_LIST: {
            return {
                ...state,
                messageList: action.payload.data
            }
        }

        default: {
            return state
        }
    }
}