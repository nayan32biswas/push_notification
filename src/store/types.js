// Common error
export const SERVER_LOST = "SERVER_LOST"

// auth types
export const REGISTRATION = "REGISTRATION"
export const LOGIN = "LOGIN"
export const LOGOUT = "LOGOUT"
export const FETCH_USERS = "FETCH_USERS"

// message
export const SEND_MESSAGE = "SEND_MESSAGE"
export const FETCH_MESSAGE = "FETCH_MESSAGE"
export const FETCH_NEW_MESSAGE = "FETCH_NEW_MESSAGE"
export const FETCH_MESSAGE_LIST = "FETCH_MESSAGE_LIST"