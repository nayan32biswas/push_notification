import axios from "axios";


function getAuthToken() {
    const token = localStorage.getItem('auth_token');
    if (token) {
        return "JWT " + token;
    }
    return null;
}
const baseApi = axios.create({
    baseURL: 'http://127.0.0.1:8000/api',
    timeout: 10000,
});

baseApi.interceptors.request.use(function (config) {
    config.headers = { ...config.headers, Authorization: getAuthToken() };
    return config;
}, function (error) {
    return Promise.reject(error);
});
baseApi.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    return Promise.reject(error);
});

export default baseApi;